#include <stdio.h>
#include <GL/glew.h>
#include "glfw/include/GLFW/glfw3.h"


int init_resources(void);
void free_resources();


/* Global:
 * Holds the program, this is the combination of a vertex and
 * fragment shader, and is to be compiled and linked succesfully. */
GLuint program;

/* Global:
 * This variable is used to pass vertices to the vertex shader.
 * The vertex shader executes per vertex */
GLint attribute_coord2d;


int main(int argc, char *argv[])
{

  GLFWwindow *window;

  /* init glfw library */
  if(!glfwInit()) {
    return 1;
  }

  /* request a window and create an opengl context on it */
  window = glfwCreateWindow(500, 500, "glfw window", NULL, NULL);
  if(!window) {
    glfwTerminate();
    return 1;
  } 

  /* makes window's context current */
  glfwMakeContextCurrent(window);


  /* Attempt to initialize glew.. */
  GLenum err = glewInit();
  if(GLEW_OK != err) {
    fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
  } else {
    fprintf(stdout, "Using GLEW version: %s\n", glewGetString(GLEW_VERSION));
  }


  /* Attempt to initialize shader resources */
  if(init_resources() != 1) {
    fprintf(stderr, "Could not initialize resources..\n");
  }


  /* loop until window is closed, this is the display function. */
  while(!glfwWindowShouldClose(window)) {

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    /* clear color buffer */
    glViewport(0, 0, width, height);
    glClearColor(1.0, 1.0, 1.0, 1.0);
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program);
    glEnableVertexAttribArray(attribute_coord2d);

    GLfloat vertices[] = {
      -0.5,  0.5,
      -0.5, -0.5,
       0.5,  0.5,

       0.5,  0.5,
      -0.5, -0.5,
       0.5, -0.5
    };

    glVertexAttribPointer(
      attribute_coord2d, // attribute
      2,                 // number of elements per vertex (x,y)
      GL_FLOAT,          // type of each element
      GL_FALSE,          // take values as-is
      0,                 // no extra data between each position
      vertices           // pointer to GLfloat array i.e GLfloat[] *
    ); 

    glDrawArrays(GL_TRIANGLES, 0, 6);
    glDisableVertexAttribArray(attribute_coord2d);

    glfwSwapBuffers(window);
    glfwPollEvents();
  }

  free_resources();

  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
}

int init_resources(void)
{
  GLint compile_ok = GL_FALSE;
  GLint link_ok = GL_FALSE;

  GLuint vs = glCreateShader(GL_VERTEX_SHADER);
  const char *vs_source =
  "#version 130\n"
  "attribute vec2 coord2d;"
  "void main(void) {"
  "  gl_Position = vec4(coord2d, 0.0, 1.0);"
  "}";

  glShaderSource(vs, 1, &vs_source, NULL);
  glCompileShader(vs);
  glGetShaderiv(vs, GL_COMPILE_STATUS, &compile_ok);
  if(0 == compile_ok) {
    fprintf(stderr, "Error in vertex shader.\n");
    return 1;
  } else {
    fprintf(stdout, "Succesfully compiled vertex shader.\n");
  }

  GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
  const char *fs_source =
    "#version 130           \n"
    "void main(void) {        "
    "  gl_FragColor[0] = 0.0; "
    "  gl_FragColor[1] = 0.0; "
    "  gl_FragColor[2] = 1.0; "
    "}";
  glShaderSource(fs, 1, &fs_source, NULL);
  glCompileShader(fs);
  glGetShaderiv(fs, GL_COMPILE_STATUS, &compile_ok);
  if (!compile_ok) {
    fprintf(stderr, "Error in fragment shader.\n");
    return 1;
  } else {
    fprintf(stdout, "Succesfully compiled fragment shader.\n");
  }

  program = glCreateProgram();
  glAttachShader(program, vs);
  glAttachShader(program, fs);
  glLinkProgram(program);
  glGetProgramiv(program, GL_LINK_STATUS, &link_ok);
  if(!link_ok) {
    fprintf(stderr, "failed to link program, status: %d\n", link_ok);
    return 1;
  }

  /* this value must match the name you want to use in the vertex
   * shader. As it is looked for with glGetAttribLocation in the
   * built program. this process is to pass the (triangle) vertices
   * to the vertex shader. */
  const char *attribute_name = "coord2d";
  attribute_coord2d = glGetAttribLocation(program, attribute_name);
  printf("attribute_coord2d value is %d\n", attribute_coord2d);
  if(attribute_coord2d == -1) {
    fprintf(stderr, "Could not bind attribute %s\n", attribute_name);
    return 1;
  }
}

void free_resources()
{
  glDeleteProgram(program);
}

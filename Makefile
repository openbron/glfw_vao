CC=gcc
CFLAGS=-Wall
LIBS=glfw/build/src/libglfw3.a -lGLEW -lglfw -lGL -lX11 -lXi -lpthread -lXrandr\
                               -lXxf86vm -lm
TARGETS=main.c
OUTPUT=program

$(OUTPUT): $(TARGETS)
	$(CC) $(TARGETS) $(LIBS) -o $(OUTPUT)

clean:
	rm program
